package mngbeans;

import if6ae.jpa.InscricaoJpa;
import if6ae.jpa.InscricaoMinicursoJpa;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@SessionScoped
public class AppBean extends PageBean {

    public AppBean() {
    }
    
    public InscricaoJpa getInscricaoJpa(){
        return new InscricaoJpa();
    }
    
    public InscricaoMinicursoJpa getInscricaoMinicursoJpa(){
        return new InscricaoMinicursoJpa();
    }
    
}
