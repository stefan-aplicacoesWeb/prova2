/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

//import inscricao.persistence.entity.Inscricao;
//import inscricao.persistence.entity.Inscricao;
import if6ae.entity.Inscricao_minicurso;
//import inscricao.persistence.entity.Inscricao_;
import if6ae.entity.Inscricao_minicurso_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

/**
 *
 * @author stefan
 */
public class InscricaoMinicursoJpa extends JpaController {

    public InscricaoMinicursoJpa() {
    }
    
    public List<Inscricao_minicurso> findInscricaoMinicursoByNumero(int numero){
        EntityManager em = null;
        try {
            em = getEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao_minicurso> cq = cb.createQuery(Inscricao_minicurso.class);
            Root<Inscricao_minicurso> rt = cq.from(Inscricao_minicurso.class);
            cq.where(cb.equal(rt.get(Inscricao_minicurso_.numero_inscricao), numero));
            TypedQuery<Inscricao_minicurso> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            if (em != null) em.close();
        }
    }
}
