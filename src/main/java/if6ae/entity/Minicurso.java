/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Stefan
 */
@Entity
@Table(name = "minicurso")
@NamedQueries({
    @NamedQuery(name = "Minicurso.findAll", query = "SELECT m FROM Minicurso m")})
public class Minicurso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo")
    private Integer codigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ministrante")
    private String ministrante;
     @Basic(optional = false)
    @NotNull
    @Column(name = "vagas")
    private Integer vagas;
    
    public Minicurso() {
    }

    public Minicurso(Integer codigo) {
        this.codigo = codigo;
    }

    public Minicurso(Integer codigo, String descricao, String ministrante, Integer vagas) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.ministrante = ministrante;
        this.vagas = vagas;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getMinistrante() {
        return ministrante;
    }

    public void setMinistrante(String ministrante) {
        this.ministrante = ministrante;
    }

    public Integer getVagas() {
        return vagas;
    }

    public void setVagas(Integer vagas) {
        this.vagas = vagas;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Minicurso)) {
            return false;
        }
        Minicurso other = (Minicurso) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

//    @Override
//    public String toString() {
//        return "inscricao.persistence.entity.Candidato[ cpf=" + cpf + " ]";
//    }    
}
