/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Stefan
 */
@Entity
@Table(name = "inscricao_minicurso")
@NamedQueries({
    @NamedQuery(name = "Inscricao_minicurso.findAll", query = "SELECT m FROM Inscricao_minicurso m")})
public class Inscricao_minicurso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numero_inscricao")
    private Integer numero_inscricao;
//    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "minicurso")
    private Integer minicurso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data_hora;
    @Basic(optional = false)
    @NotNull
    @Column(name = "situacao")
    private Integer situacao;
    
    public Inscricao_minicurso() {
    }

    public Inscricao_minicurso(Integer numero_inscricao, Integer minicurso, Date data_hora, Integer situacao) {
        this.numero_inscricao = numero_inscricao;
        this.minicurso = minicurso;
        this.data_hora = data_hora;
        this.situacao = situacao;
    }

    public Integer getNumero_inscricao() {
        return numero_inscricao;
    }

    public void setNumero_inscricao(Integer numero_inscricao) {
        this.numero_inscricao = numero_inscricao;
    }

    public Integer getMinicurso() {
        return minicurso;
    }

    public void setMinicurso(Integer minicurso) {
        this.minicurso = minicurso;
    }

    public Date getData_hora() {
        return data_hora;
    }

    public void setData_hora(Date data_hora) {
        this.data_hora = data_hora;
    }

    public Integer getSituacao() {
        return situacao;
    }

    public void setSituacao(Integer situacao) {
        this.situacao = situacao;
    }
        
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (codigo != null ? codigo.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Inscricao_minicurso)) {
//            return false;
//        }
//        Inscricao_minicurso other = (Inscricao_minicurso) object;
//        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
//            return false;
//        }
//        return true;
//    }

//    @Override
//    public String toString() {
//        return "inscricao.persistence.entity.Candidato[ cpf=" + cpf + " ]";
//    }    

}
